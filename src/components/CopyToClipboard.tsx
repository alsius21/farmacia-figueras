import { CopyIcon, CheckCircledIcon, Cross1Icon } from "@radix-ui/react-icons";
import { useRef, useState } from "react";
import clsx from "clsx";
interface Props {
  text: string;
}

export default function CopyToClipboard({ text }: Props) {
  const dialogRef = useRef<HTMLDialogElement>(null);
  const [isOpen, setIsOpen] = useState(false);
  const copyToClipboard = (text: string) => {
    navigator.clipboard
      .writeText(text)
      .then(() => {
        // Optional: Add some visual feedback
        console.log("Copied to clipboard");
        setIsOpen(true);
        setTimeout(() => {
          setIsOpen(false);
        }, 3000);
      })
      .catch((err) => {
        console.error("Failed to copy text: ", err);
      });
  };
  return (
    <>
      <button className="cursor-pointer" onClick={() => copyToClipboard(text)}>
        <CopyIcon />
      </button>
      <dialog
        ref={dialogRef}
        open={isOpen}
        className={clsx(
          "left-1/2 top-4 z-50 m-0 mt-0 flex w-max -translate-x-1/2 flex-row items-center gap-4 rounded-lg border border-blue-200 bg-white px-4 py-2 shadow-lg",
          isOpen && "fixed",
          !isOpen && "hidden",
        )}
      >
        <CheckCircledIcon className="text-blue-500" />
        <span className="text-blue-500">Copiat al portapapers</span>
        <button onClick={() => setIsOpen(false)}>
          <Cross1Icon className="text-blue-500" />
        </button>
      </dialog>
    </>
  );
}
