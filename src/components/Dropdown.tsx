import React from "react";
import * as DropdownMenu from "@radix-ui/react-dropdown-menu";

const Dropdown = ({
  title,
  subtitle,
  items,
}: {
  title: string;
  subtitle: string;
  items: { text: string; link: string }[];
}) => {
  return (
    <DropdownMenu.Root>
      <DropdownMenu.Trigger asChild>
        <div className="group flex items-center gap-2 lg:flex-col lg:gap-0">
          <button
            className="font-semibold text-white group-hover:text-blue-400 group-active:text-blue-400 lg:text-gray-500 lg:group-hover:text-blue-600"
            aria-label={title}
          >
            {title}
          </button>
          <span className="text-sm text-gray-200 group-hover:text-blue-400 lg:text-gray-800 lg:group-hover:text-blue-500">
            {subtitle}
          </span>
        </div>
      </DropdownMenu.Trigger>

      <DropdownMenu.Portal>
        <DropdownMenu.Content
          className="data-[side=top]:animate-slideDownAndFade data-[side=right]:animate-slideLeftAndFade data-[side=bottom]:animate-slideUpAndFade data-[side=left]:animate-slideRightAndFade min-w-[220px] rounded-md bg-white p-[5px] shadow-[0px_10px_38px_-10px_rgba(22,_23,_24,_0.35),_0px_10px_20px_-15px_rgba(22,_23,_24,_0.2)] will-change-[opacity,transform]"
          sideOffset={5}
        >
          {items.map((item) => (
            <DropdownMenu.Item className="data-[disabled]:text-mauve8 data-[highlighted]:bg-violet9 data-[highlighted]:text-violet1 group relative flex h-[25px] select-none items-center rounded-[3px] px-[5px] pl-[25px] text-[13px] leading-none outline-none data-[disabled]:pointer-events-none">
              <a
                key={item.link}
                href={item.link}
                className="list-none text-white hover:text-blue-400 lg:text-black lg:hover:text-blue-600"
                target={item.link.includes("https") ? "_blank" : "_self"}
              >
                {item.text}
              </a>
            </DropdownMenu.Item>
          ))}

          <DropdownMenu.Arrow className="fill-white" />
        </DropdownMenu.Content>
      </DropdownMenu.Portal>
    </DropdownMenu.Root>
  );
};

export default Dropdown;
