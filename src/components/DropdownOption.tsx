import React from "react";

const DropdownOption: React.FC<{ text: string; link: string }> = ({
  text,
  link,
}) => {
  return (
    <li className="dropdown-li p-2">
      <a
        href={link}
        className="list-none text-white hover:text-blue-400 lg:text-black lg:hover:text-blue-600"
      >
        {text}
      </a>
    </li>
  );
};

export default DropdownOption;
