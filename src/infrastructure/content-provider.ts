export interface ContentProvider {
  getStaticContent: (slug: string) => Promise<any>;
  getLiveContent: () => Promise<any>;
}
