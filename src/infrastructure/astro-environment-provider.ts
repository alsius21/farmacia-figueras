import { type IEnvironmentProvider } from "./environment-provider";

export const isCMSPreviewEnabled = () => {
  return import.meta.env.STORYBLOK_IS_PREVIEW === "yes";
};

const astroEnvironmentProvider: IEnvironmentProvider = {
  isCMSPreviewEnabled: () => isCMSPreviewEnabled(),
};

export default astroEnvironmentProvider;
