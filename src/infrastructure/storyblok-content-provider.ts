import { useStoryblokApi, getLiveStory } from "@storyblok/astro";
import type { ContentProvider } from "./content-provider";
import type { IEnvironmentProvider } from "./environment-provider";

type ContentProviderBuilder = (
  astro: any,
  environmentProvider: IEnvironmentProvider,
) => ContentProvider;

const storyblokContentProviderBuilder: ContentProviderBuilder = (
  astro: any,
  environmentProvider: IEnvironmentProvider,
) => ({
  getStaticContent: async (slug: string) => {
    const storyblokApi = useStoryblokApi();
    const { data } = await storyblokApi.get(`cdn/stories/${slug}`, {
      version: environmentProvider.isCMSPreviewEnabled()
        ? "draft"
        : "published",
    });
    return data.story?.content;
  },
  getLiveContent: async () => {
    try {
      const liveStory = await getLiveStory(astro);
      console.info("liveStory", liveStory);
      return liveStory?.content;
    } catch (error) {
      console.error("liveStory error", error);
      return null;
    }
  },
});

export { storyblokContentProviderBuilder };
