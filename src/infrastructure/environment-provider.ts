export interface IEnvironmentProvider {
  isCMSPreviewEnabled: () => boolean;
}
