export function formatDateInCatalan(date: Date): string {
  // Array of month names in Catalan
  const monthNames = [
    "Gen",
    "Feb",
    "Mar",
    "Abr",
    "Mai",
    "Jun",
    "Jul",
    "Ago",
    "Set",
    "Oct",
    "Nov",
    "Des",
  ];

  // Array of day names in Catalan
  const dayNames = ["Dg", "Dl", "Dt", "Dc", "Dj", "Dv", "Ds"];

  // Extract the day, month, and year from the date object
  const day = date.getDate();
  const month = monthNames[date.getMonth()];
  const year = date.getFullYear();
  const dayName = dayNames[date.getDay()];

  // Format the date as [Dl 21 Mar 1994]
  return `${dayName} ${day.toString().padStart(2, "0")} ${month} ${year}`;
}
