const Routes = {
  home: "home",
  "la-farmacia": "la-farmacia",
  contacte: "contacte",
  serveis: "serveis",
  membres: "membres",
  informacio: "informacio",
  guardies: "guardies",
  "atencio-farmaceutica": "atencio-farmaceutica",
  spd: "spd",
  "encarrecs-online": "encarrecs-online",
  "formules-magistrals": "formules-magistrals",
} as const;
type RouteID = (typeof Routes)[keyof typeof Routes];

export const routesTranslations: Record<RouteID, string> = {
  [Routes.home]: "Inici",
  [Routes["la-farmacia"]]: "La farmàcia",
  [Routes.serveis]: "Serveis",
  [Routes["atencio-farmaceutica"]]: "Atenció farmacèutica",
  [Routes["contacte"]]: "Contacte",
  [Routes["informacio"]]: "Informació",
  [Routes["guardies"]]: "Guàrdies",
  [Routes.membres]: "Membres",
  [Routes.spd]: "SPD",
  [Routes["encarrecs-online"]]: "Encàrrec Online",
  [Routes["formules-magistrals"]]: "Fórmules magistrals",
};
export type Item = {
  link: string;
  text: string;
  id: string;
};

export function createBreadcrumb(pathname: string): Item[] {
  const breadcrumbs = [
    { id: "home", link: "/", text: routesTranslations.home },
  ];
  if (pathname === "/") return breadcrumbs;
  if (pathname.endsWith("/")) {
    pathname = pathname.slice(0, -1);
  }
  const segments = pathname.split("/").slice(1);
  const url = { accumulator: "" };
  for (const element of segments) {
    // Handle special case such as "/" or empty string
    const path = element.toLowerCase() || "home";
    const id = Object.keys(routesTranslations).find(
      (route) => route.toLowerCase() === path,
    );
    if (!id) throw new Error(`Translation for route "${element}" not found`);
    url.accumulator += `/${element}`;
    const link = url.accumulator;
    const text = routesTranslations[id as keyof typeof routesTranslations];
    breadcrumbs.push({ id, link, text });
  }
  return breadcrumbs;
}
