export interface IContentService {
  getData: (slug: string) => Promise<any>;
}
