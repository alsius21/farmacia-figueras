import type { ContentProvider } from "@/infrastructure/content-provider";
import type { IContentService } from "./icontent-service";

type ContentServiceBuilder = (cmsProvider: ContentProvider) => IContentService;

const contentServiceBuilder: ContentServiceBuilder = (
  contentProvider: ContentProvider,
) => ({
  getData: async (slug: string) => {
    return (
      (await contentProvider.getLiveContent()) ??
      (await contentProvider.getStaticContent(slug))
    );
  },
});

export { contentServiceBuilder };
