import { expect, describe, it } from "vitest";
import { formatDateInCatalan } from "./date.utils";

describe("formatDateInCatalan", () => {
  it("should format the date in Catalan", () => {
    expect(formatDateInCatalan(new Date("07-12-2024"))).toBe("Dv 12 Jul 2024");
  });
});
