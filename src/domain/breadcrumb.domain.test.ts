import { createBreadcrumb } from "./breadcrumb.domain";
import { expect, test, describe } from "vitest";

describe("Breadcrumb creation", () => {
  test("/", () => {
    const pathname = "/";
    const expected = [{ id: "home", link: "/", text: "Inici" }];
    expect(createBreadcrumb(pathname)).toEqual(expected);
  });
  test("/la-farmacia should display Inici > La farmacia", () => {
    const pathname = "/la-farmacia";
    const expected = [
      { id: "home", link: "/", text: "Inici" },
      { id: "la-farmacia", link: "/la-farmacia", text: "La farmàcia" },
    ];
    expect(createBreadcrumb(pathname)).toEqual(expected);
  });
  test("/la-farmacia/membres should display Inici > La farmacia > Membres", () => {
    const pathname = "/la-farmacia/membres";
    const expected = [
      { id: "home", link: "/", text: "Inici" },
      { id: "la-farmacia", link: "/la-farmacia", text: "La farmàcia" },
      { id: "membres", link: "/la-farmacia/membres", text: "Membres" },
    ];
    expect(createBreadcrumb(pathname)).toEqual(expected);
  });
  test("given that it ends with a /, it should remove the /", () => {
    const pathname = "/la-farmacia/contacte/";
    const expected = [
      { id: "home", link: "/", text: "Inici" },
      { id: "la-farmacia", link: "/la-farmacia", text: "La farmàcia" },
      { id: "contacte", link: "/la-farmacia/contacte", text: "Contacte" },
    ];
    expect(createBreadcrumb(pathname)).toEqual(expected);
  });
});
