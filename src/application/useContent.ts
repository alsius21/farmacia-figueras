import { contentServiceBuilder } from "@/domain/content-service";
import astroEnvironmentProvider from "@/infrastructure/astro-environment-provider";
import { storyblokContentProviderBuilder } from "@/infrastructure/storyblok-content-provider";

const useContent = async (astro: any, slug: string) => {
  const contentProvider = storyblokContentProviderBuilder(
    astro,
    astroEnvironmentProvider,
  );
  const contentService = contentServiceBuilder(contentProvider);

  return await contentService.getData(slug);
};

export default useContent;
