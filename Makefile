SPACE_ID=288627
login:
	storyblok login
pull-components:
	storyblok pull-components --space $(SPACE_ID)
generate-types:
	storyblok generate-typescript-typedefs --sourceFilePaths ./components.${SPACE_ID}.json --destinationFilePath ./component-types-sb.d.ts