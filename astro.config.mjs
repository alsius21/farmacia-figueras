import { defineConfig } from "astro/config";
import basicSsl from "@vitejs/plugin-basic-ssl"; // Storyblok v2 requirement
import { loadEnv } from "vite";
import partytown from "@astrojs/partytown";
import {storyblok} from "@storyblok/astro";
import tailwind from "@astrojs/tailwind";
import react from "@astrojs/react";
import vercel from "@astrojs/vercel";
const env = loadEnv("", process.cwd(), "STORYBLOK");

// https://astro.build/config
export default defineConfig({
  vite: {
    plugins: [basicSsl()],
    server: {
      https: true,
    },
  },
  integrations: [
    react(),
    storyblok({
      accessToken: env.STORYBLOK_TOKEN,
      bridge: env.STORYBLOK_IS_PREVIEW === 'yes',
      livePreview: env.STORYBLOK_IS_PREVIEW === 'yes',
      /**
       * La key debe coincidir exactamente con el nombre del Blok en Storyblok para que se referencie correctamente.
       * Si no coinciden, o estás intentando referenciar un componente que no existe en Storyblok, obtendrás un error.
       * @see https://docs.astro.build/es/guides/cms/storyblok/
       */
      components: {
        page: "storyblok/Page",
        dropdown: "storyblok/Dropdown",
        hero: "storyblok/Hero",
        dropdownOption: "storyblok/DropdownOption",
        header: "storyblok/HeaderBlok",
        card: "storyblok/Card",
        section: "storyblok/Section",
        article: "storyblok/Article",
        cardList: "storyblok/CardList",
        table: "storyblok/Table",
      },
    }),
    tailwind(),
    partytown({
      forward: ["dataLayer.push"],
    }),
  ],
  output: env.STORYBLOK_IS_PREVIEW === 'yes' ? 'server' : 'static',
  adapter: vercel()
});
